package mrth.labs.java.try0;

public class Program {
   public static void main(String[] args) {
      TreeImpl<Integer> tree = new TreeImpl<>();

      tree.insert(10);
      tree.insert(7);
      tree.insert(15);
      tree.insert(13);
      tree.insert(18);
      tree.insert(11);
      tree.insert(16);
      tree.insert(20);
      tree.insert(16);

      System.out.println(tree.toString());
      System.out.println("----");

      tree.delete(12);
      System.out.println(tree.toString());
   }
}
