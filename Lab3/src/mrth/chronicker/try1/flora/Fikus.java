package mrth.chronicker.try1.flora;

import mrth.chronicker.try1.Life;

import java.util.List;

public class Fikus extends Flora {

   public Fikus() {
      name = "";
      vid = "фикус";
   }

   public Fikus(String name) {
      this.name = name;
      vid = "фикус";
   }

   @Override
   public void eat(Life life) {
      System.out.println(getName() + " не ест " + life.getName());
      System.out.println(getName() + " скушал пару фотонов");
   }

   @Override
   public Life spread(List<Life> parents) {
      return new Fikus();
   }

   @Override
   public String getName() {
      return vid + " " + name;
   }
}
