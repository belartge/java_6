package mrth.chronicker.try1.fauna.sea;

import mrth.chronicker.try1.fauna.Fauna;
import mrth.chronicker.try1.fauna.WayOfMoving;

public abstract class SeaFauna extends Fauna {
   public SeaFauna() {
      waysOfMoving.add(WayOfMoving.SWIM);
   }

   @Override
   public void move() {
      System.out.println("Рыба" + vid + " " + name + " начала двигаться");
   }
}
