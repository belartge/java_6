package mrth.chronicker.try1.fauna.sea;

import mrth.chronicker.try1.Color;
import mrth.chronicker.try1.Life;
import mrth.chronicker.try1.fauna.forest.Dog;

import java.util.List;

public class Sailfish extends SeaFauna {

   public Color color;

   public Sailfish(Color color) {
      this(color, "");
   }

   public Sailfish(Color color, String name) {
      this.name = name;
      this.color = color;
      this.vid = "Сельдь";
   }

   @Override
   public void eat(Life life) {
      System.out.println(getName() + " съела " + life.getName());
   }

   @Override
   public Life spread(List<Life> parents) {
      if (parents.size() == 1) {
         if (getClass() == Sailfish.class && parents.get(0).getClass() == Sailfish.class) {
            Color newColor = ((Sailfish) parents.get(0)).color;
            if (random.nextInt() % 4 == 0) {
               newColor = color;
            }
            return new Dog(newColor, "(Ребенок от " + ((Sailfish) parents.get(0)).name + " и " + name + ")");
         }
      }
      System.out.println("Нельзя создать ребенка");
      return null;
   }

   @Override
   public String getName() {
      return getColor() + " " + vid + " " + name;
   }

   private String getColor() {
      return getString(color);
   }

   public static String getString(Color color) {
      switch (color) {
         case BLACK: return "Черная";
         case WHITE: return "Белая";
         case YELLOW: return "Желтая";
         case GREEN: return "Зеленая";
         case RED: return "Красная";
      }
      return "Безцветная";
   }
}
