package mrth.chronicker.try1.fauna;

public enum WayOfMoving {
   SWIM,
   RUN,
   WALK,
   FLY
}