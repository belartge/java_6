package mrth.chronicker.try1.fauna.forest;

import mrth.chronicker.try1.fauna.Fauna;
import mrth.chronicker.try1.fauna.WayOfMoving;

public abstract class ForestFauna extends Fauna {
   public ForestFauna() {
      waysOfMoving.add(WayOfMoving.WALK);
   }

   @Override
   public void move() {
      System.out.println(getName() + " " + getNameOfMoving());
   }
}
