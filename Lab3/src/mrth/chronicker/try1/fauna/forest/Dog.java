package mrth.chronicker.try1.fauna.forest;

import mrth.chronicker.try1.Color;
import mrth.chronicker.try1.Life;
import mrth.chronicker.try1.fauna.Fauna;
import mrth.chronicker.try1.fauna.Predatory;
import mrth.chronicker.try1.fauna.WayOfMoving;

import java.util.List;
import java.util.Random;

import static mrth.chronicker.try1.fauna.sea.Sailfish.getString;

public class Dog extends ForestFauna implements Predatory, Runnable {

   public Color color;

   public Dog(Color color, String name) {
      this.color = color;
      vid = "Собака";
      this.name = name;
      changeWayOfMoving();
   }

   public void bark() {
      System.out.println("BARK!");
   }

   @Override
   public void eat(Fauna animal) {
      System.out.println(getName() + " сожрала " + animal.getName());
      animal = null;
   }

   @Override
   public void eat(Life life) {
      System.out.println(getName() + " съела " + life.getName());
      life = null;
   }

   @Override
   public Life spread(List<Life> parents) {
      if (parents.size() == 1) {
         if (getClass() == Dog.class && parents.get(0).getClass() == Dog.class) {
            Color newColor = ((Dog) parents.get(0)).color;
            if (random.nextInt() % 4 == 0) {
               newColor = color;
            }
            return new Dog(newColor, "(Ребенок от " + ((Dog) parents.get(0)).name + " и " + name + ")");
         }
      }
      System.out.println("Нельзя создать ребенка");
      return null;
   }

   @Override
   public String getName() {
      return getColor() + " " + vid + " " + name;
   }

   private String getColor() {
      return getString(color);
   }

   @Override
   public void run() {
      System.out.println(getName() + " бежит");
   }

   @Override
   public void changeWayOfMoving() {
      this.waysOfMoving.add(WayOfMoving.RUN);
   }
}
