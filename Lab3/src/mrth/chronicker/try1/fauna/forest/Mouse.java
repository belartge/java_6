package mrth.chronicker.try1.fauna.forest;

import mrth.chronicker.try1.Color;
import mrth.chronicker.try1.Life;

import java.util.List;

import static mrth.chronicker.try1.fauna.sea.Sailfish.getString;

public class Mouse extends ForestFauna {

   public Color color;

   public Mouse(Color color, String name) {
      this.color = color;
      vid = "Мышь";
      this.name = name;
   }

   public void peep() {
      System.out.println("Peep!");
   }

   @Override
   public void eat(Life life) {
      System.out.println(getName() + " съела " + life.getName());
   }

   @Override
   public Life spread(List<Life> parents) {
      if (parents.size() == 1) {
         if (getClass() == Mouse.class && parents.get(0).getClass() == Mouse.class) {
            Color newColor = ((Mouse) parents.get(0)).color;
            if (random.nextInt() % 4 == 0) {
               newColor = color;
            }
            return new Mouse(newColor, "(Ребенок от " + ((Mouse) parents.get(0)).name + " и " + name + ")");
         }
      }
      System.out.println("Нельзя создать потомка");
      return null;
   }

   @Override
   public String getName() {
      return getColor() + " " + vid + " " + name;
   }

   private String getColor() {
      return getString(color);
   }
}
