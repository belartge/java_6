package mrth.chronicker.try1;

import mrth.chronicker.try1.fauna.forest.Dog;
import mrth.chronicker.try1.fauna.forest.Mouse;
import mrth.chronicker.try1.fauna.sea.Sailfish;
import mrth.chronicker.try1.flora.Fikus;

import java.util.ArrayList;
import java.util.List;

public class Program {

   public static void main(String[] args) {
      Dog doggo1 = new Dog(Color.BLACK, "Шарик");
      Dog doggo2 = new Dog(Color.WHITE, "Жучка");
      Mouse mouse = new Mouse(Color.GREEN, "Мышка");
      Fikus fikus = new Fikus("Жора");
      Sailfish fish = new Sailfish(Color.YELLOW);

      Life puppy = birthExperiment(doggo1, doggo2);
      if (puppy != null) {
         System.out.println("Родился новый пес: " + ((Dog) puppy).getName());
      }

      line();

      Life whatIsThis = birthExperiment(mouse, fish);
      if (whatIsThis != null) {
         System.out.println("Родилось непонятное оно: " + whatIsThis.getName());
      }

      line();

      mouse.move();
      mouse.peep();
      mouse.eat(fikus);

      line();

      doggo2.move();
      doggo2.eat(mouse);

      line();

      doggo1.move();
      doggo1.eat(fish);

      line();
      ((Dog) puppy).bark();
   }

   public static void line() {
      System.out.println();
   }

   public static Life birthExperiment(Life animal1, Life animal2) {
      List<Life> dogs = new ArrayList<>();
      dogs.add(animal2);
      System.out.println("Пытаемся создать потомка от " + animal1.getName() + " и " + animal2.getName());
      return animal1.spread(dogs);
   }
}
